#!/usr/bin/env python
#-*- coding:utf-8 -*-
from __future__ import print_function

from PyQt4 import QtCore, QtGui
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from httplib2 import Http
import os
import os.path


import datetime

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json

SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'
store = Storage('storage.json')



class MyWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        super(MyWindow, self).__init__(parent)
        self.listWidget=QtGui.QTreeWidget()
        self.listWidget.setColumnCount(2)
        self.listWidget.setHeaderLabels(['Data','Zadanie'])

        self.pushButtonWindow = QtGui.QPushButton(self)
        self.pushButtonWindow.setText("Dodaj Wydarzenie")
        self.pushButtonWindow.clicked.connect(self.on_pushButton_clicked)

        self.pushButtonWindow3= QtGui.QPushButton(self)
        self.pushButtonWindow3.setText("Usun zaznaczone")
        self.pushButtonWindow3.clicked.connect(self.on_pushButton_clicked3)

        self.eventList = []

        self.cal = QtGui.QCalendarWidget(self)
        self.cal.setGridVisible(True)
        self.cal.move(20, 20)

    
        self.textEdit = QtGui.QLineEdit()

        self.add_toList()

        self.setGeometry(100,100,900,600)
        self.setWindowTitle('Calendar')
        self.horizontalLayout = QtGui.QHBoxLayout()

        self.verticalLayout2 = QtGui.QVBoxLayout()
        self.verticalLayout2.addWidget(self.cal)
        self.verticalLayout2.addWidget(self.textEdit)
        self.verticalLayout2.addWidget(self.pushButtonWindow)
        self.horizontalLayout.addLayout(self.verticalLayout2)

        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.addWidget(self.listWidget)
        self.verticalLayout.addWidget(self.pushButtonWindow3)

        self.gridLayout = QtGui.QGridLayout(self)
        self.gridLayout.addLayout(self.horizontalLayout,0,0)
        self.gridLayout.addLayout(self.verticalLayout,0,1)

    def add_toList(self):
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            creds = tools.run_flow(flow, store, flags) \
                    if flags else tools.run(flow, store)

        CAL = discovery.build('calendar', 'v3', http=creds.authorize(Http()))
        yesterday = datetime.datetime.today()- datetime.timedelta(1)
        now = yesterday.isoformat() + 'Z'

        eventsResult = CAL.events().list(
            calendarId='primary', timeMin=now, maxResults=10, singleEvents=True,
            orderBy='startTime').execute()
        events = eventsResult.get('items', [])
        self.eventList = events

        if not events:
            print('No upcoming events found.')
        for event in events:
            start = event['start'].get('dateTime', event['start'].get('date'))
            print(start, event['summary'])
            print(event['id'])
            string = start + ' ~~ ' + event['summary']

            item = QtGui.QTreeWidgetItem()
            item.setText(0, start )
            item.setText(1, event['summary'])
            self.listWidget.addTopLevelItem(item)
        self.listWidget.header().setResizeMode(QtGui.QHeaderView.ResizeToContents)


    @QtCore.pyqtSlot()
    def on_pushButton_clicked(self):
        item = QtGui.QTreeWidgetItem()
        item.setText(0, self.cal.selectedDate().toString('yyyy-MM-dd') )
        item.setText(1, self.textEdit.text())
        self.listWidget.addTopLevelItem(item)

        creds = store.get()
        str = self.cal.selectedDate().toString('yyyy-MM-dd') +"~~ "+ self.textEdit.text()

        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            creds = tools.run_flow(flow, store, flags) \
                    if flags else tools.run(flow, store)
        CAL = discovery.build('calendar', 'v3', http=creds.authorize(Http()))
        string = str.split("~~")

        EVENT = {
            'summary':  unicode(string[1], encoding="UTF-8"),
            'start':  {'date': unicode(string[0], encoding="UTF-8")},
            'end':    {'date': unicode(string[0], encoding="UTF-8")},
            }

        e = CAL.events().insert(calendarId='primary',
                sendNotifications=True, body=EVENT).execute()
        self.eventList.append(e)
    @QtCore.pyqtSlot()
    def on_pushButton_clicked3(self):
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            creds = tools.run_flow(flow, store, flags) \
                    if flags else tools.run(flow, store)

        CAL = discovery.build('calendar', 'v3', http=creds.authorize(Http()))
        listItems=self.listWidget.selectedItems()
        if not listItems: return
        for item in listItems:
            itemIndex=self.listWidget.indexOfTopLevelItem(item)
            self.listWidget.takeTopLevelItem(itemIndex)


            CAL.events().delete(calendarId='primary', eventId=self.eventList[itemIndex]['id']).execute()
            del self.eventList[itemIndex]

if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('MyWindow')

    main = MyWindow()
    main.show()

    sys.exit(app.exec_())
